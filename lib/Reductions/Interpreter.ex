defmodule Interpreter do
  @doc """
  Will go through the program and start parsing it, checking whether or not it is a function declaration or a statement
  For function declaration, they are added to a map with function name as the key and function declaration ast as the value
  For statements we spawn the process, sending its external reference to register it
  """

  def interpret(lst) do

    [head | tail] = lst #pattern matching list to head or tail

    case head do

      #match function
      {:func_decl_ast, {{:fn_name, {:id, fn_name}}, {:id, x}, {:params, params}, stmts }} ->  ast = FuncDeclAST.inst(fn_name, x, params, stmts)
                                                                                              FnMap.add_fn(fn_name, ast) #adding entry to function map


      #for any other statement type - these are all processes
      _ -> client_pid = spawn(fn ->  Util.ast_gen(head) |> Util.my_spawn() end ) #start the client (when not a function declaration, we can assume a process is starting)
          send(client_pid, {:client, client_pid})
          Process.sleep(1000)

    end

    if tail != [] do interpret(tail) else :finished end #call on tail
  end
end
