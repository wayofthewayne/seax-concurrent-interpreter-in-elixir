defmodule Rules do
  @moduledoc """
  Defining all the reductions rules
  """

  # Important Notes
  # The external reference will be the 'name' with which we refer to the process - it's pid
  # The internal reference for a provider will be the clients external reference - i.e the channel the provider provides to.

  ### CUT ###
  def cut(prc) do
    provider_pid = spawn_link(fn -> Util.sub(prc.ast.cont_1, prc.ast.id, prc.ext_ref) |> Util.my_spawn() end) #start the provider (Note how the internal reference (client name) is subbed - cannot use self() function - wrong context)
    #we also link the provider to the client, for future garbage collection

    send(provider_pid, {:provider, provider_pid, self()}) #note the internal reference is where it provides on - it's client (i.e self() = prc.ext_ref)

    Util.write_to_log("CUT\n") # log rule before reducing
    PrcReplicable.instance(prc.ext_ref, prc.int_ref,  Util.sub(prc.ast.cont_2, prc.ast.id, provider_pid)) |> SmallStep.reduce() #update client state reduce on client
  end

  ### RCV ###
  def recv_provider(prc) do
    Util.validate_channel_external_choice_provider(prc) #validate channel provider side

    {name1, name2} = receive do #receive blocks - can be interrupted with garbage collection
      {:names_client, x,y} -> {x,y}
    end
    cont = Util.sub(prc.ast.cont, prc.ast.var1, name1) |> Util.sub(prc.ast.var2, name2) #form continuation - Note name2 will be the client's client
    send(prc.int_ref, {:cont, cont}) #send the contiuation back to the client - through internal reference
  end
  def send_client(prc) do
    Util.validate_channel_external_choice_client(prc) #validate channel client side

    send(prc.ast.chan, {:names_client, prc.ast.var1, prc.ast.var2}) #sending our internal reference to our provider
    cont = receive do #get the continuation
      {:cont, cont} -> cont
    end

    Util.write_to_log("RCV\n")
    PrcReplicable.instance(prc.ext_ref, prc.int_ref, cont) |> SmallStep.reduce() #continue reducing
  end

  ### SND ###
  def recv_client(prc) do
    Util.validate_channel_internal_choice_client(prc)

    {name1, name2} = receive do #blocks till names are received
      {:names_provider, x,y} -> {x,y}
    end

    reduced = Util.sub(prc.ast.cont, prc.ast.var1, name1) |> Util.sub(prc.ast.var2, name2) #get the names and sub them in

    Util.write_to_log("SND\n")
    PrcReplicable.instance(prc.ext_ref, prc.int_ref, reduced) |> SmallStep.reduce() #continue reduction
  end
  def send_provider(prc) do
    Util.validate_channel_internal_choice_provider(prc)
    send(prc.int_ref, {:names_provider, prc.ast.var1, prc.ast.var2}) #provider simply sends (on the internal reference - provides to client)
  end

  ### CALL ###
  def call(prc, main_fn) do
    new_state = if main_fn.params != :nil do


      body_ast = Util.ast_gen(main_fn.fn_body)
      zipped_params = Enum.zip(prc.ast.params, [main_fn.self | main_fn.params]) #zipped the passed_params with definition dummy params

      #pass the zipped list, and perform the substitutions on it. After each sub, reduce the stopping condition, and continue with updated ast
      {new_body_ast, _ } = Enum.reduce_while(zipped_params, {body_ast, length(zipped_params)},
                              fn({to_sub, var}, {dynamic_ast, condition}) ->
                                  if condition == 0  do {:halt, {body_ast, 0}} else {:cont, {Util.sub(dynamic_ast, var, to_sub), condition-1}} end end
                            )

      PrcReplicable.instance(prc.ext_ref, prc.int_ref, new_body_ast)

    else #empty param case
      body_ast = Util.ast_gen(main_fn.fn_body)
      new_ast = Util.sub(body_ast, main_fn.self, List.first(prc.ast.params))#only need to sub in self

      PrcReplicable.instance(prc.ext_ref, prc.int_ref, new_ast)
    end

    Util.write_to_log("CALL\n") #log event
    new_state |> SmallStep.reduce() #continue reduction
  end

  ### SPL ###
  def split_client(prc) do
    send(prc.ast.chan, :split) #trigger duplication

    {pid1, pid2} = receive do
      {:split_pids, new_pid_1, new_pid_2} -> {new_pid_1, new_pid_2}
    end

    if Process.alive?(pid1) do #for case of send (executes before linking) - doesn't require garbage collection
      Process.link(pid1) #link the newly spawned providers to the current client
    end
    if Process.alive?(pid2) do
      Process.link(pid2)
    end

    new_cont = Util.sub(prc.ast.cont, prc.ast.var1, pid1) |> Util.sub(prc.ast.var2,pid2)

    Util.write_to_log("SPL\n")
    PrcReplicable.instance(prc.ext_ref, prc.int_ref, new_cont) |> SmallStep.reduce()
  end
  def split_provider(prc) do

      dupped_prc_pid = spawn(fn -> Util.my_spawn(prc.ast) end) #spawn a duplicated process
      send(dupped_prc_pid, {:provider, dupped_prc_pid, prc.int_ref})#send it's ext ref and int ref

      send(prc.int_ref, {:split_pids, prc.ext_ref, dupped_prc_pid})#return new pids to the client (For optimization provider becomes second duplicate process)

      prc |> SmallStep.reduce() #since this is the second duplicate process, it must continue reducing. Other process reduces on spawn
  end

  ### FWD ###
  def fwd_client(prc) do
    send(prc.ast.chan, {:fwd, self()}) #trigger a forward on the process

    new_prc = receive do #get the forwarding
      {:forwarded, {new_int_ref, cont}} -> PrcReplicable.instance(prc.ext_ref, new_int_ref, cont)
    end

    if Process.alive?(new_prc.int_ref) do #link this forwarded process with it's now new client
      Process.link(new_prc.int_ref)
    end

    Util.write_to_log("FWD\n") #log event
    new_prc |> SmallStep.reduce() #continue reduction
  end
  def fwd_provider(prc, forwarder) do
      #no need for channel validation in this case
      send(forwarder, {:forwarded, {prc.int_ref, prc.ast}}) #send to the forwarding process, our continuation and internal ref - who we provide to
  end

  ### CLS ###
  def close(prc) do
    Util.validate_channel_internal_choice_provider(prc)

    send(prc.int_ref, :closed)#indicate closed process
  end
  def wait(prc) do
    Util.validate_channel_internal_choice_client(prc)

    receive do #wait for closing provider, before reducing
      :closed -> Util.write_to_log("CLS\n") #log event
                 PrcReplicable.instance(prc.ext_ref, prc.int_ref, prc.ast.cont) |> SmallStep.reduce()
    end
  end

  ### CST ###
  def cast_provider(prc) do
    Util.validate_channel_internal_choice_provider(prc)

    send(prc.int_ref, {:upcast, prc.ast.type}) #cast on self - btr to use internal reference since, chan can be 'self'
  end
  def shift_client(prc) do
    Util.validate_channel_internal_choice_client(prc)

    to_sub = receive do
      {:upcast, type} -> type #receive type from cast (to perform upcasting)
    end

    new_cont = Util.sub(prc.ast.cont, prc.ast.type, to_sub) #perform the upcasting

    Util.write_to_log("CST\n")
    PrcReplicable.instance(prc.ext_ref, prc.int_ref, new_cont) |> SmallStep.reduce()
  end

  ### SHF ###
  def shift_provider(prc) do
    Util.validate_channel_external_choice_provider(prc)

    self_ref = receive do
      {:downcast, self_ref} -> self_ref #receive self
    end

    downcasted = Util.sub(prc.ast.cont, prc.ast.type, self_ref) #perform the shifting (downcast)
    send(prc.int_ref, {:cont_downcasted, downcasted}) #return the updated continuation. Provider finished
  end
  def cast_client(prc) do
    Util.validate_channel_external_choice_client(prc)

    send(prc.ast.chan, {:downcast, prc.ast.type}) #send to provider - type should be our internal reference

    new_prc = receive do
      {:cont_downcasted, continuation} ->  PrcReplicable.instance(prc.ext_ref, prc.int_ref, continuation) #form prc instance with new continuation
    end

    Util.write_to_log("SHF\n")
    new_prc |> SmallStep.reduce() #continue reducing on the client
  end

  ### SEL ###
  def select_provider(prc) do
    Util.validate_channel_internal_choice_provider(prc)
    send(prc.int_ref, {:select_prov, {:label, prc.ast.select, :var, prc.ast.var}}) #send to provider, the select label and variable
  end

  def case_client(prc) do
    Util.validate_channel_internal_choice_client(prc)

    {label, var} = receive do
      {:select_prov, {:label, select_label, :var, to_sub_var}} -> {select_label, to_sub_var} #receive label and passed channel
    end

    selected_case = Enum.find(prc.ast.cases, fn case_entry -> if case_entry.label == label do case_entry end end) #find the respective case entry, matching the label
    if selected_case == nil do #verify label
      IO.inspect("Selected label #{label} does not exist in label set")
      IO.inspect("shutting down ... #{inspect prc.ext_ref}")
      Process.exit(prc.ext_ref, :bad_select_case)
    end

    new_cont = Util.sub(selected_case.prc, selected_case.var, var) #sub in it
    Util.write_to_log("SEL\n")
    PrcReplicable.instance(prc.ext_ref, prc.int_ref, new_cont) |> SmallStep.reduce() #continue reduction
  end

  ### BRA ###
  def case_provider(prc) do
    Util.validate_channel_external_choice_provider(prc)
    {selection, var} = receive do #receive the selected branch
      {:select_prov, {:label, select, :var, var}} -> {select, var}
    end

    selected_case = Enum.find(prc.ast.cases, fn case_entry -> if case_entry.label == selection do case_entry end end) #find the respective case entry, matching the label
    if selected_case == nil do #verify label
      IO.inspect("Selected label #{selection} does not exist in label set")
      IO.inspect("shutting down ... #{inspect prc.ext_ref}")
      Process.exit(prc.ext_ref, :bad_select_case)
    end

    new_cont = Util.sub(selected_case.prc, selected_case.var, var) #sub in it
    send(prc.int_ref, {:select_cont, new_cont})#return it to client
  end
  def select_client(prc) do
    Util.validate_channel_external_choice_client(prc)
    send(prc.ast.chan, {:select_prov, {:label, prc.ast.select, :var, prc.ast.var}}) #send the selected branch

    cont = receive do #receive continuation
      {:select_cont, cont} -> cont
    end
    Util.write_to_log("BRA\n")
    PrcReplicable.instance(prc.ext_ref, prc.int_ref, cont) |> SmallStep.reduce() #reduce on client
  end

  ### DRP ###
  def drop_client(prc) do
    Process.flag(:trap_exit, true) #capture and stop propagation of message at this stage (i.e no propagation upwards)
    Process.exit(prc.ast.chan, :drop) #drop the provider (and any process underneath is automatically garbage collected)

    Util.write_to_log("DRP\n")
    PrcReplicable.instance(prc.ext_ref, prc.int_ref, prc.ast.cont) |> SmallStep.reduce()
  end

end
