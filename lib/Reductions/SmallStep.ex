defmodule SmallStep do
  @moduledoc """
  Module finds which processes can be reduced, thereby performing a small step operation
  """

  @doc """
  Find whether process is client or provider, and identify which reduction can be applied.
  First, we always check for priority messages such as split and forward since these are general reductions that can be applied to most processes
  """
  def reduce(prc) do

    #File.open("test.txt", [:append], fn file -> IO.inspect(file, prc, []) end) # dump contents to test file for debugging

    receive do #priority messages for general calls
      :split -> Rules.split_provider(prc)
      {:fwd, forwarding_ref} -> Rules.fwd_provider(prc, forwarding_ref)

    after #when no messages are present

      0 -> case prc.ast.asttype do #check for other reductions
        :process_spawn -> Rules.cut(prc)

        :recv_ast ->
          cond do
            prc.ast.chan == prc.int_ref or prc.ast.chan == 'self'-> Rules.recv_provider(prc)#start recv from provider perspective
            is_pid(prc.ast.chan) -> Rules.recv_client(prc) #start recv from client perspective
            true -> IO.inspect("Unexpected: ") #unexpected recv case
                    IO.inspect(prc)
                    Process.exit(prc.ext_ref, :bad_syntax)
          end

        :send_ast ->
          cond do
            #note the or allows the programmer to pass self as a variable. However, when starting a client, the channel can never be 'self'
            (prc.ast.var2 == prc.int_ref or prc.ast.var2 == 'self') and is_pid(prc.ast.chan)-> Rules.send_client(prc) #start send as client
            prc.ast.chan ==  prc.int_ref or (prc.ast.chan == 'self') -> Rules.send_provider(prc) #start send from provider prespective
            true -> IO.inspect("Unexpected: ")
                    IO.inspect(prc)
                    Process.exit(prc.ext_ref, :bad_syntax)
          end

        :func_call ->
          case FnMap.get_fn(prc.ast.fn_name) do #find the function

            {:ok, main_fn} ->
              if prc.int_ref == List.first(prc.ast.params) or (List.first(prc.ast.params) == 'self') do #condition for a proper function call as per reduction rules - first param must be self/int ref
                Rules.call(prc, main_fn) # reduce it

              else #bad case
                  IO.inspect("Bad Function call")
                  Process.exit(prc.ext_ref, :bad_syntax)
              end

            :error -> IO.inspect("Bad Function call or Map process not running")
                      Process.exit(prc.ext_ref, :bad_syntax)
          end

        :split_ast ->
          cond do
            is_pid(prc.ast.chan) -> Rules.split_client(prc) #split client
            true -> IO.inspect("Unexpected: ")
                    IO.inspect(prc)
                    Process.exit(prc.ext_ref, :bad_syntax)
          end

        :fwd_ast ->
          cond do
            is_pid(prc.ast.chan) -> Rules.fwd_client(prc) #fwd client
            true -> IO.inspect("Unexpected: ")
                    IO.inspect(prc)
                    Process.exit(prc.ext_ref, :bad_syntax)
          end

        :wait_ast ->
          cond do
            is_pid(prc.ast.chan) -> Rules.wait(prc) #wait client
            true -> IO.inspect("Unexpected: ")
                    IO.inspect(prc)
                    Process.exit(prc.ext_ref, :bad_syntax)
          end

        :close_ast ->
          cond do
            prc.ast.chan == 'self' or (prc.ast.chan == prc.int_ref)-> Rules.close(prc) #close is always provider
            true -> IO.inspect("Unexpected: ")
                    IO.inspect(prc)
                    Process.exit(prc.ext_ref, :bad_syntax)
          end

        :print_ast -> check_for_test = Process.whereis(:verifier)

                      if nil == check_for_test do #when no test is present
                        PrcReplicable.disp(prc.ext_ref, prc.int_ref, prc.ast) #display names
                      else
                        send(:verifier, {:to_verify, prc}) #send back to the main process for verification
                      end

        :cast_ast ->
          cond do
            prc.ast.chan == 'self' or (prc.ast.chan == prc.int_ref) -> Rules.cast_provider(prc)
            is_pid(prc.ast.chan) and (prc.ast.type == 'self' or prc.ast.type == prc.int_ref)-> Rules.cast_client(prc)
            true -> IO.inspect("Unexpected: ")
                    IO.inspect(prc)
                    Process.exit(prc.ext_ref, :bad_syntax)
          end

        :shift_ast ->
          cond do
            prc.ast.chan == 'self' or (prc.ast.chan == prc.int_ref)-> Rules.shift_provider(prc)
            is_pid(prc.ast.chan) -> Rules.shift_client(prc)
            true -> IO.inspect("Unexpected: ")
                    IO.inspect(prc)
                    Process.exit(prc.ext_ref, :bad_syntax)
          end

        :select_ast ->
          cond do
            prc.ast.chan == prc.int_ref or prc.ast.chan == 'self' -> Rules.select_provider(prc)
            is_pid(prc.ast.chan) and (prc.ast.var == prc.int_ref or prc.ast.var == 'self') -> Rules.select_client(prc)
            true -> IO.inspect("Unexpected: ")
                    IO.inspect(prc)
                    Process.exit(prc.ext_ref, :bad_syntax)
          end

        :case_ast ->
          cond do
            prc.ast.chan == 'self' or prc.ast.chan == prc.int_ref -> Rules.case_provider(prc)
            is_pid(prc.ast.chan) -> Rules.case_client(prc)
            true -> IO.inspect("Unexpected: ")
                    IO.inspect(prc)
                    Process.exit(prc.ext_ref, :bad_syntax)
          end

        :drop_ast ->
          cond do
            is_pid(prc.ast.chan) -> Rules.drop_client(prc)
            true -> IO.inspect("Unexpected: ")
                    IO.inspect(prc)
                    Process.exit(prc.ext_ref, :bad_syntax)
          end

      end #close of cases
    end

  end #close of function and module
end
