defmodule Fyp do
  @moduledoc """
  A concurrent Interpreter (executes the reduction rules) for Seax - Semi-Axiomatic Sequent Calculus
  """

    @doc """
    Perform the compilation process
    """
    def main do
      FnMap.start_link() #Start a process to hold the function declaration in map of type {fn_name, fn_decl}

      logger_pid = spawn(fn -> RuleLogger.listen([]) end) #start and register a logger process which logs the reduction rules
      Process.register(logger_pid, MyLog)

      amount_of_running_processes = Process.list() |> Enum.count() #get amount of running processes

      Lexer.readFile('Source.seax') |>  Lexer.lex() |> Parser.myParse() |> Interpreter.interpret() #compilation pipeline

      finished_execution(amount_of_running_processes) #Main thread is waiting for code execution to complete. Even though all processes are ran, they might still be executing.
      #Must wait for these to finish to shutdown
    end

    @doc """
    Perform graceful shutdown
    """
    def finished_execution(processes_amount) do
      current_amount = Process.list() |> Enum.count() #see amount of running processes at this point in time

      if(processes_amount == current_amount) do #see if all running processes have finished execution
        FnMap.stop() #stop map process
        send(MyLog, :write) #write rules to file and shutdown process
        IO.puts("Shutting down compiler...")
      else
        Process.sleep(100) #if not check again in a 100 milliseconds
        finished_execution(processes_amount)
      end
    end
end
