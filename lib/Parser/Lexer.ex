defmodule Lexer do
  @moduledoc """
  Perform Lexing from text file input
  """

  @doc """
  Read from file into a string and return charlist
  """

  @spec readFile([char]) :: charlist()
  def readFile(filePath) do
    case File.read(filePath) do
      {:ok, text} -> text <> " eof" |> to_charlist() #append programmer defined end of file to the read string - leex does not implement $ regex
      _ -> IO.puts("Error reading from file!") #error handle
    end
  end

  @doc """
  Perform lexing using leex tool, returns tokens
  """
  @spec lex(charlist()) :: [:tokens]
  def lex(code) do
    case :Rules.string(code) do #parses a charlist into tokens
      {:ok, tokens, _} -> tokens
      {:error, {line_no, :Rules, {:illegal, char_err}}, _} ->  "Error on line #{line_no}. Unexpected #{char_err}" #error handle
    end
  end

end
