defmodule Parser do
  @moduledoc """
  Perform the parsing operations using yecc tool
  """

  @doc """
  Call parse through the tool, and return the parsed content
  """
  @spec myParse(list) :: [tuple()]
  def myParse(tokens) do
    case :ParseRules.parse(tokens) do
      {:ok, parsed_cotent} -> parsed_cotent
      {:error, {line_no, :ParseRules, reason}} -> "Error on line #{line_no}. Reason: #{reason} token"
    end
  end

end
