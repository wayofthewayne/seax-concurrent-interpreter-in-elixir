defmodule PrcReplicable do
  @moduledoc "Defines the structure of a 'process' i.e prc(N,gamma,P)"

  use TypedStruct #Defines a struct and it's types

  @typedoc "Will define a process structure"
  typedstruct do
    field :ext_ref, pid()
    field :int_ref, List.t(), default: 'self'
    field :ast, AST.ast #AST are in the form of typed structs and represent the executing process P
  end

@doc """
Instantiate the structure
"""
  def instance(external_ref, internal_ref, ast) do
    %PrcReplicable{ext_ref: external_ref, int_ref: internal_ref, ast: ast}
  end

  @doc """
  Display function
  """
  def disp(ext_ref, int_ref, ast) do
    IO.puts("external reference: #{inspect ext_ref}")
    IO.puts("internal reference: #{inspect int_ref}")
    PrintAST.disp(ast.name_1,ast.name_2)
    IO.puts("------------------------------------------------------\n")
  end
end
