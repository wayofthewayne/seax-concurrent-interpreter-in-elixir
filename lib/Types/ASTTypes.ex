defmodule AST do
  @moduledoc """
  Defining type ast. This is the data structure used to hold the executing process P.
  """

  @typedoc """
  ast type is defined in terms of typedstructs allowing us to construct an 'abstract AST'. Contrete AST becomes a struct
  Type only used for documenation purposes
  """
  @type ast :: SendAST | RecvAST | SpawnAST | FuncDeclAST | FuncCallAST | SplitAST | struct() | FwdAST | WaitAST
  | CloseAST | CastAST | ShiftAST | SelectAST | CaseEntryAST| CaseAST | DropAST | PrintAST

end

defmodule PrintAST do
  @moduledoc """
  This is not a construct of Seax but a tool for verifying correctness.
  """
  use TypedStruct

  typedstruct do
    field :asttype, atom(), default: :print_ast
    field :name_1, Charlist.t(), default: "N/A"
    field :name_2, Charlist.t(), default: "N/A"
    field :chan, Charlist.t(), default: "N/A"
  end

  def inst(name1, name2) do
    %PrintAST{name_1: name1, name_2: name2}
  end

  def inst() do
    %PrintAST{}
  end

  def disp(name_1, name_2) do
    IO.puts("Name 1: #{inspect name_1}, Name 2: #{inspect name_2}")
  end

end

defmodule SendAST do
  @moduledoc """
  Define the structure for a send statement AST
  """
  use TypedStruct

  typedstruct do
    field :asttype, atom(), default: :send_ast #construct type set as default
    field :chan, Charlist.t() | pid()
    field :var1, Charlist.t() #bound variables
    field :var2,  Charlist.t()
  end

  @doc """
  Function instantiate type
  """
  @spec inst(charlist(), charlist(), charlist() ) :: SendAST.t()
  def inst(x,y,z) do
    %SendAST{chan: x, var1: y, var2: z}
  end
end

defmodule RecvAST do
  @moduledoc """
  Define the structure of a receive statement AST
  """

  use TypedStruct

  typedstruct do
    field :asttype, atom(), default: :recv_ast
    field :var1, Charlist.t()
    field :var2, Charlist.t()
    field :chan,  Charlist.t()
    field :cont, struct() #recv stores continuation
  end

  @spec inst(charlist(), charlist(), charlist() , AST.ast) :: RecvAST.t()
  def inst(x,y,z,stmt) do
    %RecvAST{var1: x, var2: y, chan: z, cont: stmt }
  end
end


defmodule SpawnAST do
  @moduledoc """
  Define AST structure for spawn statement (i.e new)
  """

  use TypedStruct

  typedstruct do
    field :asttype, atom(), default: :process_spawn
    field :id, Charlist.t()
    field :cont_1, struct() #spawn holds two statements
    field :cont_2,  struct()
  end

  @spec inst(charlist(), AST.ast, AST.ast) :: SpawnAST.t()
  def inst(id,stmt1,stmt2) do
    %SpawnAST{id: id, cont_1: stmt1, cont_2: stmt2}
  end
end

defmodule FuncDeclAST do
  @moduledoc """
  Defining the AST structure for a function declaration
  """

  use TypedStruct

  typedstruct do
    field :asttype, atom(), default: :func_decl
    field :fn_name, Charlist.t()
    field :self, Charlist.t()
    field :params, List.t()
    field :fn_body, Tuple.t() #fn_body statements are not processed at the declaration point. Body is kept as a tuple
  end

  @spec inst(charlist(), charlist(), List.t(), Tuple.t()) :: FuncDeclAST.t()
  def inst(fn_name, id, params, fn_body) do
    %FuncDeclAST{fn_name: fn_name, self: id, params: params, fn_body: fn_body}
  end

end

defmodule FuncCallAST do
  @moduledoc """
  Defining the AST structure for a function call
  """

  use TypedStruct

  typedstruct do
    field :asttype, atom(), default: :func_call
    field :fn_name, Charlist.t()
    field :params, List.t()
  end

  @spec inst(charlist(), List.t()) :: FuncCallAST.t()
  def inst(fn_name, params) do
    %FuncCallAST{fn_name: fn_name, params: params}
  end
end

defmodule SplitAST do
  @moduledoc """
  Defining the AST structure for split
  """
      use TypedStruct

      typedstruct do
      field :asttype, atom(), default: :split_ast
      field :var1, Charlist.t()
      field :var2, Charlist.t()
      field :chan, Charlist.t()
      field :cont, struct()
      end

      @spec inst(charlist(), charlist(), charlist(), AST.ast) :: SplitAST.t()
      def inst(id1,id2,id3,stmt) do
      %SplitAST{var1: id1, var2: id2, chan: id3, cont: stmt}
      end
   end

defmodule FwdAST do
    use TypedStruct

    typedstruct do
      field :asttype, atom(), default: :fwd_ast
      field :chan, Charlist.t()
    end

    def inst(id) do
      %FwdAST{chan: id}
    end
end

defmodule CloseAST do
  use TypedStruct

  typedstruct do
    field :asttype, atom(), default: :close_ast
    field :chan, Charlist.t()
  end

  def inst(id) do
    %CloseAST{chan: id}
  end
end

defmodule WaitAST do
  use TypedStruct

  typedstruct do
    field :asttype, atom(), default: :wait_ast
    field :chan, Charlist.t()
    field :cont, struct()
  end

  def inst(id, cont) do
    %WaitAST{chan: id, cont: cont}
  end
end

defmodule ShiftAST do
  use TypedStruct

  typedstruct do
    field :asttype, atom(), default: :shift_ast
    field :chan, Charlist.t()
    field :type, Charlist.t()
    field :cont, struct()
  end

  def inst(type, chan, cont) do
    %ShiftAST{type: type, chan: chan, cont: cont}
  end
end

defmodule CastAST do
  use TypedStruct

  typedstruct do
    field :asttype, atom(), default: :cast_ast
    field :chan, Charlist.t()
    field :type, Charlist.t()
  end

  def inst(chan, type) do
    %CastAST{chan: chan, type: type}
  end
end

defmodule SelectAST do
  use TypedStruct

  typedstruct do
    field :asttype, atom(), default: :select_ast
    field :chan, Charlist.t()
    field :select, Charlist.t()
    field :var, Charlist.t()
  end

   def inst(chan, select, var) do
     %SelectAST{chan: chan, select: select, var: var}
   end
end

defmodule CaseEntryAST do # AST for each specific case entry, present in the full case statement
  use TypedStruct

  typedstruct do
    field :label, Charlist.t()
    field :var, Charlist.t()
    field :prc, struct()
  end

  def inst(label, var, prc) do
    %CaseEntryAST{label: label, var: var, prc: prc}
  end
end

defmodule CaseAST do #AST structure to hold the full cases statement
  use TypedStruct

  typedstruct do
    field :asttype, atom(), default: :case_ast
    field :chan, Charlist.t()
    field :cases, List.t() #list of structs composed of case entry asts
  end

  def inst(chan, cases) do
    %CaseAST{chan: chan, cases: cases}
  end
end

defmodule DropAST do
  use TypedStruct

  typedstruct do
    field :asttype, atom(), default: :drop_ast
    field :chan, Charlist.t()
    field :cont, struct()
  end

  def inst(chan, cont) do
    %DropAST{chan: chan, cont: cont}
  end
end
