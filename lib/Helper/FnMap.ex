defmodule FnMap do
  @moduledoc """
  Process to start a map to hold all the declared functions
  """
  use Agent
  @doc """
  Start empty map
  """
  def start_link do
    Agent.start_link(fn -> %{} end, name: __MODULE__)
  end

  @doc """
  get a function from the map
  """
  def get_fn(function) do
    Agent.get(__MODULE__, fn(state) -> Map.fetch(state,function) end)
  end

  @doc """
  Add a function to the map
  """
  def add_fn(fn_name, fn_body) do
    Agent.update(__MODULE__, fn(state) -> Map.put(state, fn_name, fn_body) end)
  end

  @doc """
  Stop the process from running
  """
  def stop do
    Agent.stop(__MODULE__)
  end
end
