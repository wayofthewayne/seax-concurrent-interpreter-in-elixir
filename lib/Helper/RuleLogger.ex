defmodule RuleLogger do
  @moduledoc """
  Logs the executed reduction rules to a file.
  Logger can be set to a finite or infinite logger.
  Finite logger needs to be used when handling infinite process streams to cap the amount, else logger will never shut down. (best used in test cases)
  """

  @doc """
  Log list has no limit for logging rules
  """
  def infinite_list(list,rule_type) do
    new_list = list ++ [rule_type] #append rule to list
    listen(new_list)
  end


  @doc """
  Logging is list is made finite in this case for handling infinite streams
  """
  def finite_list(list, rule_type) do
    new_list = list ++ [rule_type] #append rule to list

    if length(new_list) < 10000 do #max amount of rules logged
        listen_finite(new_list) #look for next message
    else
      {:ok, file} = File.open("log.txt", [:write, :truncate])

                    # Write rules to file (forced)
                    IO.write(file, list)
                    File.close(file) #close file

                    check_for_test = Process.whereis(:verifier) #check if we are in a test setting
                    if check_for_test != nil do send(:verifier, :logged) end #indicate that writing is complete to test
      end
  end


  @doc """
  Keep listening for logging messages
  """
  def listen(list) do
    receive do
      {:rule, rule_type} -> infinite_list(list,rule_type)

     :write -> # Open the file in write mode
                {:ok, file} = File.open("log.txt", [:write, :truncate])

                # Write rules to file
                IO.write(file, list)
                File.close(file) # close file and process shutdowns

      :write_test -> {:ok, file} = File.open("log.txt", [:write, :truncate])
                      # Write rules to file
                      IO.write(file, list)
                      File.close(file) #close file

                      send(:verifier, :logged) #indicate that writing is complete

  end
end

@doc """
Same as above with a finite amount of messages logged,
after which point the logger is shut down
"""
def listen_finite(list) do
      receive do
        {:rule, rule_type} -> finite_list(list,rule_type)

       :write -> # Open the file in write mode
                  {:ok, file} = File.open("log.txt", [:write, :truncate])

                  # Write rules to file
                  IO.write(file, list)
                  File.close(file) # close file and process shutdowns

        :write_test -> {:ok, file} = File.open("log.txt", [:write, :truncate])
                        # Write rules to file
                        IO.write(file, list)
                        File.close(file) #close file

                        send(:verifier, :logged) #indicate that writing is complete

    end
  end
end
