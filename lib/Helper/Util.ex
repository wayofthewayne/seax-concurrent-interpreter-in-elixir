defmodule Util do

  @moduledoc """
  This module will contain helper functions used through out the code base
  """

  @doc """
  Function will generate the ast for the target language
  """
  def ast_gen(stmt) do #note important to restrict to only statements (due to recursive nature of statements -- look at recv)

  ast = case stmt do #pattern match parser output and convert it to AST structure

    {:send_ast, {:id, x}, {:id, y},{:id, z}} -> SendAST.inst(x,y,z)
    #--------------------------------------------------------------------------------------------------------------#

    {:receive_ast, {:id, x}, {:id, y},{:id, z}, continuation} ->
      continuation_ast = ast_gen(continuation) #get the AST of the continuation

      if continuation_ast != :not_found do
          RecvAST.inst(x,y,z,continuation_ast) #set the recv ast
      else :not_found end
    #--------------------------------------------------------------------------------------------------------------#

    {:process_spawn, {:id, x}, {:processes,  spawn_A, spawn_B} } ->
      ast1 = ast_gen(spawn_A) #get ast for both client and provider
      ast2 = ast_gen(spawn_B)

        if ast1 != :not_found and  ast2 != :not_found do #assert they exist

        #generate the spawn ast
        SpawnAST.inst(x,ast1,ast2)
        else :not_found end
    #--------------------------------------------------------------------------------------------------------------#

      {:fn_call_ast, {{:fn_name , {:id, fn_name}}, {:params,  param_list}}} ->
        case FnMap.get_fn(fn_name) do#find the function
          {:ok, ret_fn} ->
            cond do
              #no parameters passed to call
              param_list == :nil -> IO.inspect("Function call #{fn_name} should at least contain self as parameter")
                                      :not_found

              #function call is valid
              length(param_list) == 1 and ret_fn.params == :nil -> FuncCallAST.inst(fn_name,param_list)
              length(param_list) == length(ret_fn.params)+1 -> FuncCallAST.inst(fn_name,param_list)

              #unmatching param length
              true -> IO.inspect("Call function #{fn_name} params do not match")
                      :not_found
            end

        #case function not found
          :error -> IO.inspect("Call function #{fn_name} was not declared")
                    :not_found
      end
      #--------------------------------------------------------------------------------------------------------------#

      {:split_ast, {:id, x}, {:id, y}, {:id, z}, continuation} ->
        cont_ast = ast_gen(continuation)
        if cont_ast != :not_found do
          SplitAST.inst(x,y,z,cont_ast)
        end
      #--------------------------------------------------------------------------------------------------------------#

      {:print_ast, {:id, name1}, {:id,name2}} -> PrintAST.inst(name1,name2)
      #--------------------------------------------------------------------------------------------------------------#

      {:fwd_ast, {:id, name}} -> FwdAST.inst(name)
      #--------------------------------------------------------------------------------------------------------------#

      {:wait_ast, {:id, name}, continuation} ->
        cont_ast = ast_gen(continuation)
        if cont_ast != :not_found do
          WaitAST.inst(name,cont_ast)
        end
      #--------------------------------------------------------------------------------------------------------------#

      {:close_ast, {:id, name}} -> CloseAST.inst(name)
      #--------------------------------------------------------------------------------------------------------------#

      {:shift_ast, {:id, type}, {:id, chan}, cont} ->
        cont_ast = ast_gen(cont)

        if(cont_ast != :not_found) do
          ShiftAST.inst(type,chan,cont_ast)
        else
          :not_found
        end
      #--------------------------------------------------------------------------------------------------------------#

      {:cast_ast, {:id, chan}, {:id, type}} -> CastAST.inst(chan, type)
      #--------------------------------------------------------------------------------------------------------------#

      {:select_ast, {:id, chan}, {:id, selection}, {:id, var}} -> SelectAST.inst(chan, selection, var)
      #--------------------------------------------------------------------------------------------------------------#

      {:case_ast, {:id, chan}, {:cases, case_list}} ->
        list = [] #create a list to store individual case entries

        list = for {{:id, label}, {:id, var}, case_stmt}  <- case_list do #go through the cases
            case_cont = ast_gen(case_stmt)#validate the case entry statement & identify it

            if case_cont != :not_found do
              list ++ CaseEntryAST.inst(label, var, case_cont) #appending each one in a list
            end
        end

        CaseAST.inst(chan, list) #form the full case ast
      #--------------------------------------------------------------------------------------------------------------#

      {:drop_ast, {:id, chan}, continuation} ->
        ast_cont = ast_gen(continuation)

        if(ast_cont != :not_found) do
          DropAST.inst(chan, ast_cont)
        else
          :not_found
        end

      #--------------------------------------------------------------------------------------------------------------#

      _ -> IO.inspect("Cannot identify structure #{stmt}")
        :not_found
  end

  ast
end

@doc """
When spawing a process, we set it's external and internal reference
"""
def my_spawn(ast) do
  if ast == :not_found do
    Process.exit(self(), :bad_ast)
  end

  prc = receive do #set the provider or client
    {:provider, ext_ref, int_ref} ->  PrcReplicable.instance(ext_ref, int_ref, ast)
    {:client, ext_ref} -> PrcReplicable.instance(ext_ref, ext_ref, ast)
  end
  prc |> SmallStep.reduce() #we start reducing once a process spawns
end

@doc """
  inform logger of rule
"""
def write_to_log(rule_name) do
    log_pid = Process.whereis(MyLog) #get log pid

    if log_pid != nil do
      case Process.alive?(log_pid) do #check if log is running
        true -> send(log_pid, {:rule, rule_name}) #log rule
        false -> :log_stopped
      end
    end
end


@doc """
 Validate the channel from the provider side, when provider is internal choice (does not recieve self or internal reference) i.e SND
"""
def validate_channel_internal_choice_provider(prc) do
  send(prc.int_ref, {:channel_check, prc.ext_ref}) #provider sends to client it s external ref

  receive do #waits for client confirmation that this is correct channel
      :wrong_provider -> Process.sleep(100)
        validate_channel_internal_choice_provider(prc) #if not this is retried
      :right_provider -> :ok #else proceed
    end
end

@doc """
 Validate the channel from the client side, when client is internal choice (does not send self) i.e RCV
"""
def validate_channel_internal_choice_client(prc) do
  receive do
      {:channel_check, pid} -> case pid == prc.ast.chan do #when provider pid matches working channel
                                    true -> send(pid, :right_provider) #inform provider
                                    false -> send(pid, :wrong_provider)
                                             validate_channel_internal_choice_client(prc) #wait for retry
                                end
    end
end

@doc """
 Validate the channel from the client side, when client is external choice (will send self or internal reference)
"""
def validate_channel_external_choice_client(prc) do
  send(prc.ast.chan, {:channel_check, prc.ext_ref}) #send provider, external reference

  receive do #waits for client confirmation that this is correct channel
      :wrong_client -> Process.sleep(100)
        validate_channel_external_choice_provider(prc) #if not this is retried
      :right_client -> :ok #else proceed
    end
end

@doc """
 Validate the channel from the provider side, when provider is external choice (will recieve self)
"""
def validate_channel_external_choice_provider(prc) do
  receive do
      {:channel_check, pid} -> case pid == prc.int_ref do #when client pid matches internal reference
                                    true -> send(pid, :right_client) #inform client
                                    false -> send(pid, :wrong_client)
                                             validate_channel_external_choice_client(prc) #wait for retry
                                end
    end
end


@doc """
substitute into the ast, the new variable
var_to_sub is the variable being replaced with the replacement_var
"""
def sub(ast_stmt, var_to_sub, replacement_var) do

    case ast_stmt.asttype do #identify the ast
      :print_ast -> cond do
        var_to_sub == ast_stmt.name_1 -> ast_stmt |> Map.put(:name_1,replacement_var) #alter the struct. Recall, typed struct, is struct at runtime (MAP)
        var_to_sub == ast_stmt.name_2 -> ast_stmt |> Map.put(:name_2, replacement_var)
        true -> ast_stmt #return unchanged statement
      end
    #--------------------------------------------------------------------------------------------------------------#

      :process_spawn -> cond do
        var_to_sub == ast_stmt.id -> new_stmt = ast_stmt |> Map.put(:id, replacement_var) #when we alter the binding parameter, we much change also the parameter in the continuation (renaming)
                                     ret_1 = sub(new_stmt.cont_1, var_to_sub, replacement_var)
                                     ret_2 = sub(new_stmt.cont_2, var_to_sub, replacement_var)
                                     SpawnAST.inst(new_stmt.id, ret_1, ret_2) #updated spawn instance

        true -> ret_1 = sub(ast_stmt.cont_1, var_to_sub, replacement_var) #else just effect the changes in the continuation (note that both must be renamed)
                ret_2 = sub(ast_stmt.cont_2, var_to_sub, replacement_var)
                SpawnAST.inst(ast_stmt.id, ret_1,ret_2)

      end
    #--------------------------------------------------------------------------------------------------------------#

      :send_ast -> cond do
        var_to_sub == ast_stmt.chan -> ast_stmt |> Map.put(:chan, replacement_var) #sub into the required field
        var_to_sub == ast_stmt.var1 -> ast_stmt |> Map.put(:var1,replacement_var)
        var_to_sub == ast_stmt.var2 -> ast_stmt |> Map.put(:var2, replacement_var)
        true -> ast_stmt #return unchanged statement
      end
    #--------------------------------------------------------------------------------------------------------------#

      :recv_ast -> cond do
        var_to_sub == ast_stmt.var1 -> ast_stmt |> Map.put(:var1, replacement_var)
        var_to_sub == ast_stmt.var2 -> ast_stmt |> Map.put(:var2,replacement_var)
        var_to_sub == ast_stmt.chan -> ast_stmt |> Map.put(:chan, replacement_var)
        true -> new_cont = sub(ast_stmt.cont,var_to_sub,replacement_var) #calling sub on inner body of recv, if no matches are found. Note: it is possible the continuation remains unchanged
                RecvAST.inst(ast_stmt.var1,ast_stmt.var2,ast_stmt.chan,new_cont) #generate updated ast
      end
    #--------------------------------------------------------------------------------------------------------------#
      #Note func call doesn't allow sub for function name
      :func_call ->
        if Enum.member?(ast_stmt.params, var_to_sub) == false do ast_stmt else

          index = Enum.with_index(ast_stmt.params, fn element, index -> {element, index} end) |> #get index of all the parameters
          Enum.reduce_while(var_to_sub, fn {val,index}, var -> if val == var, do: {:halt, index}, else: {:cont, var} end) #and then get the index at the replacement

          new_params = List.replace_at(ast_stmt.params, index,replacement_var)#generate new list with subbed in param
          ast_stmt |> Map.put(:params, new_params) #update
        end
    #--------------------------------------------------------------------------------------------------------------#

      :split_ast -> cond do
        var_to_sub == ast_stmt.chan -> ast_stmt |> Map.put(:chan, replacement_var)
        var_to_sub == ast_stmt.var1 -> ast_stmt |> Map.put(:var1,replacement_var)
        var_to_sub == ast_stmt.var2 -> ast_stmt |> Map.put(:var2, replacement_var)
        true -> new_cont = sub(ast_stmt.cont, var_to_sub, replacement_var)
                SplitAST.inst(ast_stmt.var1, ast_stmt.var2, ast_stmt.chan, new_cont)
      end
    #--------------------------------------------------------------------------------------------------------------#

      :fwd_ast -> cond do
        var_to_sub == ast_stmt.chan -> ast_stmt |> Map.put(:chan, replacement_var)
        true -> ast_stmt
      end
    #--------------------------------------------------------------------------------------------------------------#

      :wait_ast -> cond do
        var_to_sub == ast_stmt.chan -> ast_stmt |> Map.put(:chan, replacement_var)
        true -> new_cont = sub(ast_stmt.cont, var_to_sub, replacement_var)
                WaitAST.inst(ast_stmt.chan, new_cont)
      end
    #--------------------------------------------------------------------------------------------------------------#

      :close_ast -> cond do
        var_to_sub == ast_stmt.chan -> ast_stmt |> Map.put(:chan, replacement_var)
        true -> ast_stmt
      end
    #--------------------------------------------------------------------------------------------------------------#

     :shift_ast -> cond do
      var_to_sub == ast_stmt.chan -> ast_stmt |> Map.put(:chan, replacement_var)
      var_to_sub == ast_stmt.type -> ast_stmt |> Map.put(:type, replacement_var)
      true -> new_cont = sub(ast_stmt.cont, var_to_sub, replacement_var)
              ShiftAST.inst(ast_stmt.type, ast_stmt.chan, new_cont)
     end
    #--------------------------------------------------------------------------------------------------------------#

     :cast_ast -> cond do
      var_to_sub == ast_stmt.chan -> ast_stmt |> Map.put(:chan, replacement_var)
      var_to_sub == ast_stmt.type -> ast_stmt |> Map.put(:type, replacement_var)
      true -> ast_stmt
     end
    #--------------------------------------------------------------------------------------------------------------#

     :select_ast -> cond do #label is static
      var_to_sub == ast_stmt.chan -> ast_stmt |> Map.put(:chan, replacement_var)
      var_to_sub == ast_stmt.var -> ast_stmt |> Map.put(:var, replacement_var)
      true -> ast_stmt
     end

    #--------------------------------------------------------------------------------------------------------------#
     :case_ast -> cond do
      var_to_sub == ast_stmt.chan -> ast_stmt |> Map.put(:chan, replacement_var)
      true -> new_cases = []
              new_cases = for case_entry <- ast_stmt.cases do #for each case statement, we go through the continuation process, and replace instances of the variable. (allows us to use same variable in various branches)
                subbed_cont =  sub(case_entry.prc, var_to_sub, replacement_var)
                new_cases ++ CaseEntryAST.inst(case_entry.label, case_entry.var, subbed_cont) # Note how, in a case statement, we do not alter the label, or bound channel
              end

              ast_stmt |> Map.put(:cases, new_cases)
     end

    #--------------------------------------------------------------------------------------------------------------#
     :drop_ast -> cond do
      var_to_sub == ast_stmt.chan -> ast_stmt |> Map.put(:chan, replacement_var)
      true -> new_cont = sub(ast_stmt.cont, var_to_sub, replacement_var)
              DropAST.inst(ast_stmt.chan, new_cont)
     end

  end
end
end
