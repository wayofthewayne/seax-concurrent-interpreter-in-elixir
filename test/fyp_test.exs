defmodule FypTest do
  use ExUnit.Case
  doctest Fyp

  test "cut rule" do
    try do
      Test_Helper.test_start_up('TestCasesSource/cut.txt') #Test Start Up

      #Test specific assertions

      #since we start two print statements, we have two receive statements
      stmt_1 = receive do
        {:to_verify, print_stmt_1} -> assert is_pid(print_stmt_1.ast.name_1) == true #we expect the x to be replaced with a pid
                                      print_stmt_1
      end
      stmt_2 = receive do
        {:to_verify, print_stmt_2} -> assert is_pid(print_stmt_2.ast.name_1) == true
                                      print_stmt_2
      end

      if stmt_1.ast.name_2 == 'provider' do #providers internal reference should link back to client (cannot determine order of the print statements)
        assert stmt_1.int_ref == stmt_2.ext_ref
      else
        if stmt_2.int_ref == stmt_1.ext_ref do #case when stmt_2 is the provider
          assert true == true
        else
          assert true == false
        end
      end

        # log file validation
        bool = Test_Helper.validate_log("CUT")
        assert bool

      rescue
        e in ExUnit.AssertionError ->
          Process.unregister(MyLog) #When failing a test capture the error, to unregister the process. This prevents further tests from failing
          raise e #re-raise the error so test still fails
      end
  end

  test "snd rule" do
    try do
      Test_Helper.test_start_up('TestCasesSource/snd.txt')

        receive do #perform assertion
          {:to_verify, print_stmt} -> assert print_stmt.ast.name_1 == 'payload'
                                      assert print_stmt.ast.name_2 == 'cont_channel'
        end

      bool_ret = Test_Helper.validate_log("SND")
      assert bool_ret

    rescue
      e in ExUnit.AssertionError ->
        Process.unregister(MyLog)
        raise e
    end
  end

  #testing rcv rule where self is hardcoded
  test "rcv hardcoded rule" do
    try do
      Test_Helper.test_start_up('TestCasesSource/rcv_hardcoded.txt')

        receive do #perform assertion
          {:to_verify, print_stmt} -> assert print_stmt.ast.name_1 == 'payload'
                                      assert print_stmt.ast.name_2 == 'self'
        end

        bool_ret = Test_Helper.validate_log("RCV")
        assert bool_ret

    rescue
      e in ExUnit.AssertionError ->
        Process.unregister(MyLog)
        raise e
    end
  end

  test "rcv" do
    try do
      Test_Helper.test_start_up('TestCasesSource/rcv.txt')

        receive do #perform assertion
          {:to_verify, print_stmt} -> assert print_stmt.ast.name_1 == 'payload'
                                      assert print_stmt.ast.name_2 == print_stmt.int_ref #verify self through continuation passing style
        end

        bool_ret = Test_Helper.validate_log("RCV")
        assert bool_ret

    rescue
      e in ExUnit.AssertionError ->
        Process.unregister(MyLog)
        raise e
    end
  end

  test "call rule" do
    try do
      Test_Helper.test_start_up('TestCasesSource/call.txt')

      receive do #perform assertion
        {:to_verify, print_stmt} -> assert print_stmt.ast.name_1 == 'payload_1' #perform the send action through a function call
                                    assert print_stmt.ast.name_2 == 'payload_2'
      end

      bool_ret = Test_Helper.validate_log("CALL")
      assert bool_ret

    rescue
      e in ExUnit.AssertionError ->
        Process.unregister(MyLog)
        raise e
    end
  end

  #this test will test out the send, recv, call constructs all together including nested cases
  test "one_place_buffer test" do
    try do
      Test_Helper.test_start_up('TestCasesSource/one_place_buffer.txt')

      receive do #perform assertion
        {:to_verify,  print_stmt} -> assert print_stmt.ast.name_1 == 'updated' #get the name passed to the server (new server name)
                                    assert Process.alive?(print_stmt.ast.name_2) == true #server is up and running
      end
      bool_ret = Test_Helper.validate_log("CUT\nCUT\nCALL\nRCV\nCUT\nSND\nCALL") #one_place_buffer should execute all these rules
      assert bool_ret

    rescue
      e in ExUnit.AssertionError ->
        Process.unregister(MyLog)
        raise e
    end
  end

  test "cls test" do
    try do
      #Start-up
      Process.register(self(), :verifier)#register the test under the name verifyier

      logger_pid = spawn(fn -> RuleLogger.listen([]) end) #start and register a logger
      Process.register(logger_pid, MyLog)

      FnMap.start_link()

      amount_of_running_processes = Process.list() |> Enum.count() #get amount of running processes

      #start interpreter
      Lexer.readFile('TestCasesSource/cls.txt') |>  Lexer.lex() |> Parser.myParse() |> Interpreter.interpret()

      receive do #perform assertion
        {:to_verify,  print_stmt} -> assert print_stmt.ast.asttype == :print_ast # verify we are in the continuation
                                    assert (Process.list() |> Enum.count()) == amount_of_running_processes #we ensure that no more processes are living. Note that print process dies after sending to verifier
      end

      bool_ret = Test_Helper.validate_log("CLS")
      assert bool_ret

    rescue
      e in ExUnit.AssertionError ->
        Process.unregister(MyLog)
        raise e
    end
  end

  test "fwd test" do
    try do
      Test_Helper.test_start_up('TestCasesSource/fwd.txt')

      receive do #perform assertion
        {:to_verify,  print_stmt} -> assert print_stmt.ast.name_1 == print_stmt.int_ref #recall through cut out subbed variable should be the same as our internal reference (since print is provider)
                                    assert print_stmt.int_ref == print_stmt.ext_ref #however the client's external reference is the same as the providers internal refernce. Since we forward this should be same
      end
      bool_ret = Test_Helper.validate_log("FWD")
      assert bool_ret

    rescue
      e in ExUnit.AssertionError ->
        Process.unregister(MyLog)
        IO.puts("\n Why this test Failed (A message from the author): \n
         Since we spawn a print provider along with a forward client, it could be the case that the print provider returns to test before the forward interaction.\n
         This results in not altering the pid of the print hence failing the test. This is a result of the concurrency and from the print not being part of the Seax language. \n
         This is a purely random occurence and cannot be replicated even under the same seed. Moreover, there is not much ways in preventing this rare occurence. \n
         This test was left in the suite since it still appropriately tests for a working forward and is one of the few means of doing so. \n
         Rerunning will most likely not replicate this issue and result in a passed test, meaning the forward works as intended.")
        raise e
    end
  end


  test "spl" do
    try do
      Test_Helper.test_start_up('TestCasesSource/split_simple.txt')

      for _i <- 1..3 do # we expect to receive three total messages (in any order) - Note if three messages are not received test runs indefinitely
        receive do
          {:to_verify, print_stmt} ->

            if is_pid(print_stmt.ast.name_1) do # this will be the split continuation print, hence we expect the return of two pids
              assert is_pid(print_stmt.ast.name_1) == true
              assert is_pid(print_stmt.ast.name_2) == true
              assert print_stmt.ast.name_1 != print_stmt.ast.name_2 # ensure they are two seperate processes

            else #else we handle the case of the print statement found within the function (this will occur twice)

              assert print_stmt.ast.name_1 == 'payload' #should carry the payload and continuation
              assert print_stmt.ast.name_2 == 'self'
            end
        end
      end

      bool_ret = Test_Helper.validate_log("SPL")
      assert bool_ret

    rescue
      e in ExUnit.AssertionError ->
        Process.unregister(MyLog)
        raise e
    end
  end

  test "spl with sub tree" do
    try do
      Test_Helper.test_start_up('TestCasesSource/split_subtree.txt')

      for _i <- 1..3 do # we expect to receive three total messages (in any order)
        receive do
          {:to_verify, print_stmt} ->

            if is_pid(print_stmt.ast.name_1) do # this will be the split continuation print, hence we expect the return of two pids
              assert is_pid(print_stmt.ast.name_1) == true
              assert is_pid(print_stmt.ast.name_2) == true
              assert print_stmt.ast.name_1 != print_stmt.ast.name_2

            else #else we handle the case of the print statement found within the function (this will occur twice)

              assert print_stmt.ast.name_1 == 'updated' #should carry the payload and continuation
              assert Process.alive?(print_stmt.ast.name_2) == true #continuation is the pid of the server, which we verify that it is still running
            end
        end
      end

      bool_ret = Test_Helper.validate_log("SPL")
      assert bool_ret

    rescue
      e in ExUnit.AssertionError ->
        Process.unregister(MyLog)
        raise e
    end
  end

  test "sel" do
    try do
      Test_Helper.test_start_up('TestCasesSource/sel.txt')

      receive do #ensure channel was passed
        {:to_verify, print_stmt} -> assert print_stmt.ast.name_1 == 'cont_channel'
      end

      bool_ret = Test_Helper.validate_log("CUT\nSEL\nCUT\nCLS") #ensure correct path was taken
      assert bool_ret

    rescue
      e in ExUnit.AssertionError ->
        Process.unregister(MyLog)
        raise e
    end
  end

  test "bra" do
    try do
      Test_Helper.test_start_up('TestCasesSource/bra.txt')

      receive do #ensure channel was passed
        {:to_verify, print_stmt} -> assert print_stmt.ast.name_1 == 'self'
      end

      bool_ret = Test_Helper.validate_log("CUT\nBRA\nCUT\nSND") #ensure correct path was taken
      assert bool_ret

    rescue
      e in ExUnit.AssertionError ->
        Process.unregister(MyLog)
        raise e
    end
  end

  #NOTE: This test may produce an invalid destintation warning (not error)
  #This occurs under the rare case, where the logger is terminated but process may have already attempted to send to the logger, before the logger is able to inform of its termination.
  #The test still passes in this case as expected. This is also a random occurance as a result of concurrency, which even under the same seed may produce different results

  #RESOLVED: The above was resolved by using the actual pid rather than the registed process name. Elixir allows the sending of messages to none-existing processes as long as it is not a registed one.
  test "relay_gate" do
    try do
      Process.register(self(), :verifier)#register the test under the name verifyier

      logger_pid = spawn(fn -> RuleLogger.listen_finite([]) end) #start a finite logger, since the bit stream is infinite
      Process.register(logger_pid, MyLog)

      FnMap.start_link() #Hold the function declaration in map of type {fn_name, fn_decl}
      Lexer.readFile('TestCasesSource/relay_gate.txt') |>  Lexer.lex() |> Parser.myParse() |> Interpreter.interpret()

      for _i <- 1..50 do #process arbitrary finite amount of bit stream
        receive do #check that correct bits were passed
          {:to_verify, print_stmt} -> assert  print_stmt.ast.name_1 == 'one'
                                      assert  print_stmt.ast.name_2 == 'one'
        end
      end

      FnMap.stop()
      {:ok,file_content} =
        receive do
          :logged -> File.read("log.txt") #check file contents
        end

        bool_ret = String.contains?(file_content, "CUT\nCUT\nCUT\nCALL\nBRA\nBRA") #infinite sequence of reductions
        assert bool_ret

      rescue
        e in ExUnit.AssertionError ->
          Process.unregister(MyLog)
          raise e
      end
  end

  test "cst_shf" do
    try do
      Test_Helper.test_start_up('TestCasesSource/cst_shf.txt')

      receive do
        {:to_verify, print_stmt} -> assert print_stmt.ast.name_1 == 'm' #check that modality m has been passed
      end

      bool_ret = Test_Helper.validate_log("CUT\nCUT\nSHF\nCST") #ensure correct path was taken
      assert bool_ret

    rescue
      e in ExUnit.AssertionError ->
        Process.unregister(MyLog)
        raise e
    end
  end

  test "drp_opb" do
    try do
      amount_of_running_processes = Process.list() |> Enum.count() #get amount of running processes

      Test_Helper.test_start_up('TestCasesSource/drp_opb.txt')

        receive do #perform assertion
          {:to_verify,  print_stmt} -> print_stmt
        end

        bool_ret = Test_Helper.validate_log("CUT\nCUT\nCALL\nRCV\nCUT\nSND\nDRP")
        assert bool_ret

        current_running_processes = Process.list() |> Enum.count()
        assert current_running_processes == amount_of_running_processes

      rescue
        e in ExUnit.AssertionError ->
          Process.unregister(MyLog)
          raise e
      end
    end

    test "drp_tree" do
      try do

        Test_Helper.test_start_up('TestCasesSource/drp_tree.txt')

        receive do #check ending print is carried out
          {:to_verify,  print_stmt} -> assert print_stmt.ast.name_1 == 'the'
                                      assert print_stmt.ast.name_2 == 'end'
        end

        receive do #ensure other print is not performed
          {:to_verify,  _print_stmt} -> assert false
        after
          500 -> assert true
        end

        bool_ret = Test_Helper.validate_log("CUT\nDRP")
        assert bool_ret

      rescue
        e in ExUnit.AssertionError ->
          Process.unregister(MyLog)
          raise e
      end
    end

end
