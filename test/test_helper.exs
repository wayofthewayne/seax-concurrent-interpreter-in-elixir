ExUnit.start()

defmodule Test_Helper do
  @moduledoc """
  Helper functions for the test suite
  """

  def test_start_up(filename) do
      Process.register(self(), :verifier)#register the test under the name verifyier (allow's prints processes to return)

      logger_pid = spawn(fn -> RuleLogger.listen([]) end) #start and register a logger
      Process.register(logger_pid, MyLog)

      FnMap.start_link() #Hold the function declaration in map of type {fn_name, fn_decl}
      Lexer.readFile(filename) |>  Lexer.lex() |> Parser.myParse() |> Interpreter.interpret()
  end

  def validate_log(validation) do
    FnMap.stop() #stop function map process

    send(MyLog, :write_test) #write contents to file, and shutdown logger process

    {:ok,file_content} =
      receive do
        :logged -> File.read("log.txt") #check file contents
      end

      String.contains?(file_content, validation) #return boolean indicating if rule was present in the log
  end
 end
