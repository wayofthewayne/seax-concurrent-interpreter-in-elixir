Nonterminals 
program select_stmt send_stmt close_stmt det_stmt id statement wait_stmt cast_stmt acc_stmt acq_stmt recv_stmt rel_stmt shift_stmt params fn_decl case_stmt cases case_entry 
prc_spawn fn_decs fn_call stmts split_stmt fwd_stmt drop_stmt print_stmt.

Terminals 
'<' 'COMMA' '>' 'SEND' 'ID' 'DOT' 'CLOSE' 'DET' 'WAIT' 'SEMICLN' 'CAST' 'ACC' 'ARW' 'ACQ' 'RECV' 'REL' 'SHIFT' 'O_PARA' 'C_PARA' 'EQUAL' 'IMPL' 'CASE' 'NEW' 'SPLIT' 'FWD' 'DROP' 'PRINT'. 

Rootsymbol program. %start parsing from the program non terminal 

Endsymbol '$end'. %indicates the token, that will signal to the parser, the end of the parsing process 

%Some general notes about yecc 
%The rules have the following structure 
%Non-terminal -> order of terminals or Non-terminals : erlang code defining the ast structure 
%$n will replace what the parser matched in the n th position of the tokens for that rule 

 
program -> fn_decs: '$1'. %we expect a program to being with fn declarations 

fn_decs -> fn_decl fn_decs: ['$1' | '$2'].  %this is a list consisting of singular function decl 
fn_decs -> stmts : '$1'. %once no more functions are present begin parsing statements (added to the same list)

stmts -> statement stmts: ['$1' | '$2']. %statements is a list consisting of singular statement rule 
stmts -> statement: ['$1']. %at least one statement must be present 

%All statement types are found below 
statement -> select_stmt: '$1'.
statement -> send_stmt: '$1'.
statement -> close_stmt: '$1'. 
statement -> det_stmt: '$1'. 
statement -> wait_stmt: '$1'.
statement -> cast_stmt: '$1'. 
statement -> acc_stmt: '$1'.
statement -> acq_stmt: '$1'.  
statement -> recv_stmt: '$1'. 
statement -> rel_stmt: '$1'. 
statement -> shift_stmt: '$1'.
statement -> case_stmt: '$1'. 
statement -> fn_call: '$1'. 
statement -> prc_spawn: '$1'.
statement -> fwd_stmt: '$1'.  
statement -> print_stmt: '$1'.
statement -> split_stmt: '$1'. 
statement -> drop_stmt: '$1'. 


%parameters are defined as a list of id's each followed by a comma. The list my potentially be empty 
params -> id 'COMMA' params: [extract_params('$1') | '$3']. % | is used to concat the list 
params -> id: [extract_params('$1') ]. 
params -> '$empty': nil.

%Rules and AST structure for function definition, function call and process spawn 
fn_decl -> id 'ARW' id 'O_PARA' params 'C_PARA' 'EQUAL' statement 'SEMICLN': {'func_decl_ast', {{'fn_name', '$3'}, '$1', {'params', '$5'}, '$8' }}.
fn_call -> id 'O_PARA' params 'C_PARA': {'fn_call_ast', {{'fn_name' , '$1'}, {'params', '$3'}}}.
prc_spawn -> id 'ARW' 'NEW' statement 'SEMICLN' statement: {'process_spawn', '$1', {'processes', '$4', '$6'}}. 

%The cases will be a list of case entries, which must have at least one entry 
cases -> case_entry cases: ['$1' | '$2']. 
cases -> case_entry: ['$1']. 
case_entry -> id '<' id '>' 'IMPL' statement: {'$1', '$3', '$6'}. %defining the structure of a case entry 

%Here the statement structures are defined (the order of the tokens), and after the colon, the AST structure is then defined. 
send_stmt -> 'SEND' id '<' id 'COMMA' id '>': {'send_ast', '$2', '$4', '$6'}.
select_stmt -> id 'DOT' id '<' id '>': {'select_ast', '$1', '$3', '$5'}.
close_stmt -> 'CLOSE' id: {'close_ast', '$2'}.
det_stmt -> 'DET' id '<' id '>': {'det_ast', '$2', '$4'}.
wait_stmt -> 'WAIT' id 'SEMICLN' statement: {'wait_ast', '$2', '$4'}.
cast_stmt -> 'CAST' id '<' id '>': {'cast_ast', '$2', '$4'}.
acc_stmt -> id 'ARW' 'ACC' id 'SEMICLN' statement: {'accepts_ast', '$1', '$4', '$6'}.
acq_stmt -> id 'ARW' 'ACQ' id 'SEMICLN' statement: {'acquire_ast', '$1', '$4', '$6'}.
rel_stmt -> id 'ARW' 'REL' id 'SEMICLN' statement: {'release_ast', '$1', '$4', '$6'}.
recv_stmt -> '<' id 'COMMA' id '>' 'ARW' 'RECV' id 'SEMICLN' statement: {'receive_ast', '$2', '$4', '$8', '$10'}. 
shift_stmt -> id 'ARW' 'SHIFT' id 'SEMICLN' statement: {'shift_ast', '$1', '$4', '$6'}.
case_stmt -> 'CASE' id 'O_PARA' cases 'C_PARA':  {'case_ast', '$2', {'cases', '$4'}}.
split_stmt -> '<' id 'COMMA' id '>' 'ARW' 'SPLIT' id 'SEMICLN' statement: {'split_ast', '$2', '$4', '$8', '$10'}.
fwd_stmt -> 'FWD' id: {'fwd_ast', '$2'}.
drop_stmt -> 'DROP' id 'SEMICLN' statement: {'drop_ast', '$2', '$4'}.

print_stmt -> 'PRINT' 'O_PARA' id 'COMMA' id 'C_PARA': {'print_ast', '$3', '$5'}.

id -> 'ID': {'id', extract_lexeme('$1')}. %Id is defined as a two tuple 

Erlang code. 

%Removing attributes that are not needed. We only need the lexeme at this stage. 
extract_lexeme({_Token, _Line, Value}) -> Value.  
extract_params({_id, Param}) -> Param.
