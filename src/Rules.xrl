Definitions.
%Below are the regex that define keywords, and other tokens 

%ID alternative [a-z][0-9a-zA-Z_]*

WHITESPACE = [\s\t\n\r]
COMMENT = [#][a-zA-Z\s\t\r]*[\n]
SEND = send
CAST = cast
SHIFT = shift
DETACH = det
ACCEPT = acc
ACQUIRE = acq
CASE = case
RELEASE = rel
CLOSE = close
WAIT = wait
RECIEVE = recv
ID = [a-z][0-9a-z_]* 
ARROW = <-
IMPLICATION = =>
EQUAL = = 
NEW = new 
SPLIT = split 
FWD = fwd 
DROP = drop 
PRINT = print 
EOF = eof

Rules.
%Here we define how each token will look like, based on the supplied regex on the LHS 

{SEND}  : {token, {'SEND',TokenLine}}.
{CAST} : {token, {'CAST', TokenLine}}.
{SHIFT} : {token, {'SHIFT', TokenLine}}.
{DETACH} : {token, {'DET', TokenLine}}.
{ACCEPT} : {token, {'ACC', TokenLine}}.
{ACQUIRE} : {token, {'ACQ', TokenLine}}.
{CASE} : {token, {'CASE', TokenLine}}.
{RELEASE} : {token, {'REL', TokenLine}}.
{CLOSE} : {token, {'CLOSE', TokenLine}}.
{WAIT} : {token, {'WAIT', TokenLine}}.
{RECIEVE} : {token, {'RECV', TokenLine}}.
{ARROW} : {token, {'ARW', TokenLine}}.
{IMPLICATION} : {token, {'IMPL', TokenLine}}.
{NEW} : {token, {'NEW', TokenLine}}. 
{EQUAL} : {token, {'EQUAL', TokenLine}}. 
{SPLIT} : {token, {'SPLIT', TokenLine}}.
{PRINT} : {token, {'PRINT', TokenLine}}. 
{FWD} : {token, {'FWD', TokenLine}}.
{DROP} : {token, {'DROP', TokenLine}}. 

\; : {token, {'SEMICLN', TokenLine}}.
\. : {token, {'DOT', TokenLine}}.
\, : {token, {'COMMA', TokenLine}}. 
\< : {token, {'<', TokenLine}}.
\> : {token, {'>', TokenLine}}. 
\( : {token, {'O_PARA', TokenLine}}.
\) : {token, {'C_PARA', TokenLine}}. 

{COMMENT} : skip_token. %comments are not returned by the lexer 
{WHITESPACE}+ : skip_token. %WhiteSpace token is not returned by the lexer 
{EOF} : {end_token, {'$end', TokenLine}}. %EOF token indicates that no more tokens are expected 
{ID} : {token, {'ID', TokenLine, TokenChars}}. %identifier token best left for the end, to prevent mistmatches 

Erlang code. 
