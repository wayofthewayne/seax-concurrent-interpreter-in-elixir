# Fyp

**Concurrent Interpreter for Seax (Semi-Axiomatic Sequent Calculus) - asynchronous channel passing language, based on adjoint logic intended to generalize binary session types. Seax guarantee's session fidelity and deadlock freedom**

## Running 
Firstly, ensure that elixir is installed on your system. Version 1.13.4 was used. One also needs the Erlang OTP compiler. Version 24.3.4.2 was used in this case. 
Install hex to be able to install the dependencies by using "mix hex.install 'VERSION'". This code was developed on VERSION=2.0.0
In the FYP folder type "mix deps.get" to install required depencies 

Navigate to 'fyp-main' folder (outside lib directory) and type "iex -S mix"
If this does not work to compile, type "iex.bat -S mix"
When the erlang interactive shell is loaded type Fyp.main() to run the interpreter  

## NOTE WHEN RUN FAILS
## In case of a failed run. Close the iex terminal by pressing crtl c, and selecting Y. Recompile and run. 
## If this is not followed the program will fail to run because of an already regsited process, the logger. 

To run the tests instead of "iex -S mix" run "mix test" and all the tests are automatically executed 


## NOTE: After execution of a 'command' the interpreter is made to sleep, hence producing a staggared output so that the output is more visibly clear. 
## This effect can be removed by removing line 22 from interpreter.ex. i.e Process.sleep(1000)

## File structure 

In the src folder one is able to find the .xrl and .yrl files. These are used by the leex and yecc tools respectively. They are responsible for the lexer and parser rules. 
They are compiled into erlang source files (.erl) and then ran with elixir. 

Under the lib folder one may find all the source files for elixir. 
The lib folder consists of a folder called "Helper" containing any utility functions or modules.
The "Parser" folder contains all the parsing and lexing code written in elixir to use the leex and yecc tools. 
The "Types" folder defines the process expression of Seax, and defines the AST struct types for storing the executing process 
Lastly, "Reduction" contains all the code in relation to the reduction rules, the small step algorithm and the interpreter.  

The "fyp.ex" module is the main file of the source code.

In the outer directory (fyp folder) one should be able to see the Source.seax file. Here one can write the source code in the target language (Seax) so that it is interpreted
The "log.txt" file will contain output of the logger, hence all the executed rules.

Under the folder TestCasesSource, one can find the source code in the target language on which the test cases are based on. 
Each test case is named in respect to the source file that it executes. 
The test cases can be found in the "test" folder, under the file name "fyp_test.exs". The "test_helper.exs" contains helper functions for the test cases. 

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `fyp` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:fyp, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at <https://hexdocs.pm/fyp>.

